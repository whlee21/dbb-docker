# Docker Compose service for DBB

```sh
git submodule init
git submodule update
cd dbb-node
./gradlew jibDockerBuild
cd ../dbb-signer
./gradlew jibDockerBuild
cd ..
docker-compose -f dbb.yml up [-d]
```